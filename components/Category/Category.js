import Image from 'next/image';
import Link from 'next/link';

export default function Category() {
  return (
    <div className="m-12">
      <div className="pt-10 pb-12 border-black container-default lg:border-b md:py-17">
        <div className="flex flex-col mb-5 md:items-center xl:pr-7 lg:flex-row lg:mb-14">
          <h2 className="text-4xl mb-4 font-bold leading-10 md:text-12 flex-1">
            Unearth your potential.
          </h2>
          <p className="flex-1 text-base font-light leading-6 xl:text-xl md:text-left max-w-prose">
            We brought fashion down to earth with products that work wonders.
            Shop our highly curated selection of the best in styles.
          </p>
        </div>
        <div className="grid items-center justify-center grid-cols-1 mb-5 md:mb-7 md:h-80 md:grid-cols-autoFit md:max-h-32 lg:fill-40 lg:max-h-52 xl:max-h-72 gap-x-5 gap-y-5">
          <Link href="/products/pria">
            <div className="group relative w-full md:h-full flex flex-col items-start justify-center md:justify-end h-24 rounded-md md:rounded-md overflow-hidden">
              <div className="absolute z-20 w-full h-full gradient-black"></div>
              <div className="absolute z-0 object-cover w-full h-full duration-500 group-hover:scale-110">
                <Image
                  src="/man.jpg"
                  loading="lazy"
                  width={800}
                  height={500}
                ></Image>
              </div>
              <p className="z-20 text-xl md:text-base lg:text-xl pl-6 md:pl-2.5 md:pb-2.5 lg:px-6.5 lg:pb-5.5 font-medium leading-6 text-white  md:leading-7">
                Pria
              </p>
            </div>
          </Link>

          <Link href="/products/wanita">
            <div className="group relative w-full md:h-full flex flex-col items-start justify-center md:justify-end h-24 rounded-md md:rounded-md overflow-hidden">
              <div className="absolute z-20 w-full h-full gradient-black"></div>
              <div className="absolute z-0 object-cover w-full h-full duration-500 group-hover:scale-110">
                <Image
                  src="/woman.jpg"
                  loading="lazy"
                  width={800}
                  height={500}
                ></Image>
              </div>
              <p className="z-20 text-xl md:text-base lg:text-xl pl-6 md:pl-2.5 md:pb-2.5 lg:px-6.5 lg:pb-5.5 font-medium leading-6 text-white  md:leading-7">
                Wanita
              </p>
            </div>
          </Link>
        </div>

        <Link href="/products">
          <div className="btn-arrow flex !justify-center mb-5">
            <span>Shop All</span>
            <div className="absolute p-0.5 bg-white rounded-full right-2  md:top-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M5 12H19"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
                <path
                  d="M12 5L19 12L12 19"
                  stroke="black"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
              </svg>
            </div>
            <div className="absolute hidden p-1 bg-black rounded-full group-hover:block right-2 top-1 md:top-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M5 12H19"
                  stroke="white"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
                <path
                  d="M12 5L19 12L12 19"
                  stroke="white"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
              </svg>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
}
