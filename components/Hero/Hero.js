import Link from 'next/link';
import Image from 'next/image';

export default function Hero() {
  return (
    <div className="container-default">
      <div className="relative flex flex-col items-end lg:m-12 pb-10 pt-7 max-w-fit md:pt-0 md:pb-10 md:flex-row md:gap-10 lg:mt-7 md:min-h-96 border-b border-black">
        <div className="absolute hidden top-6 w-32 lg:top-0 lg:w-24 md:left-0  md:block">
          <Image src="/minilogo.svg" alt="logo" width={180} height={180} />
        </div>

        <div className="flex flex-col w-full lg:max-w-81 xl:max-w-1/3">
          <h1 className=" pb-3.5 text-4xl leading-none font-bold md:text-6xl lg:pb-6 xl:text-7xl">
            Menjadi lebih fashion.
          </h1>
          <p className="pb-4 text-base xl:text-lg lg:pb-7">
            We’re your home for holistc clothing, known for quality products the
            most aesthetic-obsessed people on the planet.
          </p>
          <Link href="/products">
            <button className="btn-arrow">
              <span>Shop All Products</span>
              <div className="absolute p-0.5 bg-white rounded-full right-2 md:top-2">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <path
                    d="M5 12H19"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  ></path>
                  <path
                    d="M12 5L19 12L12 19"
                    stroke="black"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  ></path>
                </svg>
              </div>
            </button>
          </Link>
        </div>
        <div className="h-full">
          <div className="h-full pt-6 lg:pt-0">
            <div className="relative flex flex-row w-fit h-full gap-1 border-2 border-none">
              <div className="relative">
                <div className="grid w-full h-full">
                  <div className="w-full h-fit overflow-hidden rounded-md">
                    <picture>
                      <img
                        className="object-cover md:h-[33rem] hover:scale-105 transform transition duration-500 ease-in-out"
                        src="/banner.jpg"
                      ></img>
                    </picture>
                  </div>
                </div>
              </div>
              <div className="hidden pr-32 lg:w-44 lg:block">
                <div className="absolute h-auto">
                  <div className="object-cover w-[150px] h-auto">
                    <Image
                      src="/logo.png"
                      alt="logo"
                      height={2000}
                      width={600}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
