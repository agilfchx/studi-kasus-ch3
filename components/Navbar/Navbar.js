import Link from 'next/link';
import { useState } from 'react';
import CartModal from '../CartModal/CartModal';

export default function Navbar() {
  const [isClicked, setIsClicked] = useState(false);
  const handleClicked = () => {
    setIsClicked(!isClicked);
  };

  return (
    <div className="shopify-section sticky top-0 bg-white z-40 overflow-visible">
      <div className="z-10 bg-white border-b border-black">
        <header className="relative flex items-center container-default md:block h-header-mobile lg:h-header-desktop">
          <div
            className="flex-1 block md:hidden cursor-pointer"
            onClick={handleClicked}
          >
            <side-drawer-button htmlFor="menu">
              {isClicked ? (
                <>
                  <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"></div>
                  <div className="fixed top-0 left-0 w-fit md:w-1/2 h-full z-50 px-16 pt-32 pb-8 text-white transition duration-500 bg-black md:hidden ">
                    <div className="mb-10">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-x-lg"
                        viewBox="0 0 16 16"
                      >
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
                      </svg>
                    </div>
                    <nav
                      aria-label="Mobile Navigation"
                      className="h-full overflow-auto"
                    >
                      <div className="text-2xl">
                        <Link href="/products" role="menuitem">
                          <button className="block py-2 whitespace-nowrap hover:underline underline-offset-4">
                            Shop All
                          </button>
                        </Link>
                        <Link href="/about" role="menuitem">
                          <button className="block py-2 whitespace-nowrap hover:underline underline-offset-4">
                            About
                          </button>
                        </Link>
                        <Link
                          href="/"
                          className="block py-2 whitespace-nowrap hover:underline underline-offset-4"
                          role="menuitem"
                        >
                          <button className="block py-2 whitespace-nowrap hover:underline underline-offset-4">
                            Contact
                          </button>
                        </Link>
                      </div>
                      <div className="mt-10 text-lg">
                        <Link href="#" role="menuitem">
                          <button className="block py-1.5 whitespace-nowrap hover:underline underline-offset-4">
                            Locations
                          </button>
                        </Link>
                        <a href="#" role="menuitem">
                          <button className="block py-1.5 whitespace-nowrap hover:underline underline-offset-4">
                            Order Ahead
                          </button>
                        </a>
                      </div>
                    </nav>
                  </div>
                </>
              ) : (
                <svg
                  className=""
                  width="14"
                  height="12"
                  viewBox="0 0 14 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M0 1C0 0.447715 0.447715 0 1 0H13C13.5523 0 14 0.447715 14 1C14 1.55228 13.5523 2 13 2H1C0.447715 2 0 1.55228 0 1Z"
                    fill="black"
                  ></path>
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M0 6C0 5.44772 0.447715 5 1 5H13C13.5523 5 14 5.44772 14 6C14 6.55228 13.5523 7 13 7H1C0.447715 7 0 6.55228 0 6Z"
                    fill="black"
                  ></path>
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M0 11C0 10.4477 0.447715 10 1 10H13C13.5523 10 14 10.4477 14 11C14 11.5523 13.5523 12 13 12H1C0.447715 12 0 11.5523 0 11Z"
                    fill="black"
                  ></path>
                </svg>
              )}
            </side-drawer-button>
          </div>
          <left-menu
            enableanimation="false"
            animationstartposition="0"
            offset="228"
          >
            <nav
              className="flex items-center h-header-mobile lg:h-header-desktop"
              aria-label="Primary Navigation"
            >
              <Link className="md:mr-4 lg:mr-8 " href="/" aria-label="home">
                <img
                  className="w-48 mt-4 cursor-pointer"
                  src="/logo-horizontal.png"
                />
              </Link>
              <Link
                className="h-full"
                id="dropdown-menu-button-shop"
                href="/products"
                htmlFor="dropdown-menu-shop"
              >
                <button
                  slot="anchor"
                  role="menuitem"
                  className="hidden h-full font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
                  aria-haspopup="true"
                  aria-controls="dropdown-menu-shop"
                  aria-expanded="false"
                >
                  Products
                </button>
              </Link>
              <Link
                href="/about"
                className="hidden font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
              >
                <button
                  slot="anchor"
                  role="menuitem"
                  className="hidden h-full font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
                  aria-haspopup="true"
                  aria-controls="dropdown-menu-shop"
                  aria-expanded="false"
                >
                  About Us
                </button>
              </Link>
              <Link
                href=""
                className="hidden font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
              >
                <button
                  slot="anchor"
                  role="menuitem"
                  className="hidden h-full font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
                  aria-haspopup="true"
                  aria-controls="dropdown-menu-shop"
                  aria-expanded="false"
                >
                  Contact
                </button>
              </Link>
            </nav>
          </left-menu>
          <nav
            className="top-0 right-0 flex items-center justify-end flex-1 md:absolute h-header-mobile lg:h-header-desktop"
            aria-label="Secondary Navigation"
          >
            <Link
              href=""
              className="hidden font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
            >
              <button
                slot="anchor"
                role="menuitem"
                className="hidden h-full font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
                aria-haspopup="true"
                aria-controls="dropdown-menu-shop"
                aria-expanded="false"
              >
                Locations
              </button>
            </Link>
            <Link
              href=""
              className="hidden font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
            >
              <button
                slot="anchor"
                role="menuitem"
                className="hidden h-full font-medium md:px-2 lg:px-4 md:block whitespace-nowrap hover:underline underline-offset-4"
                aria-haspopup="true"
                aria-controls="dropdown-menu-shop"
                aria-expanded="false"
              >
                Order Ahead
              </button>
            </Link>
            <side-drawer-button
              htmlFor="sidecart"
              className="flex cursor-pointer md:pl-4"
            >
              <CartModal />
            </side-drawer-button>
          </nav>
        </header>
      </div>
      <dropdown-menu></dropdown-menu>
    </div>
  );
}
