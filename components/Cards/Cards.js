import Image from 'next/image';

export default function Cards({ name, price, image, id, desc }) {
  const numberDot = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  return (
    <div
      className="w-80 p-6 bg-white border rounded-xl shadow-xl hover:shadow-2xl hover:scale-105 transition-all transform duration-500"
      key={id}
    >
      <picture>
        <img
          className="w-72 object-cover rounded-t-md"
          src={image}
          alt={name}
        />
      </picture>

      <div className="mt-4">
        <h1 className="text-2xl font-bold text-gray-700">{name}</h1>
        <p className="text-gray-500 mt-2">{desc}</p>
        <div className="mb-2 flex justify-between">
          <span className="block text-xl font-semibold text-gray-700 pt-2">
            Rp{numberDot(price)}
          </span>
        </div>
      </div>
    </div>
  );
}
