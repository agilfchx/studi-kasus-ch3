import { useState, useEffect } from 'react';
import Image from 'next/image';
import Cart from '../../public/cart.svg';
import Close from '../../public/close.svg';
import data from '../../data';
import toast from 'react-hot-toast';

export default function CartModal() {
  const [isCart, setIsCart] = useState(false);
  const [cartData, setCartData] = useState([]);
  const [lenTotal, setLenTotal] = useState(0);

  const handleCart = () => {
    setIsCart(!isCart);
  };

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartData(cart);
  }, [isCart]);

  useEffect(() => {
    setLenTotal(cartData.length);
  }, [cartData]);

  const numberDot = (number) => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  };

  const localData = cartData.map((item) => {
    const dataItem = data.find((data) => data.name === item.name);
    return {
      ...dataItem,
      idCart: item.idCart,
      size: item.size,
    };
  });

  const totalPrice = localData.reduce((sum, item) => {
    return sum + item.price * 1;
  }, 0);

  const deleteItem = (idCart) => {
    const cart = JSON.parse(localStorage.getItem('cart'));
    const newCart = cart.filter((item) => item.idCart !== idCart);
    localStorage.setItem('cart', JSON.stringify(newCart));
    toast('Item deleted.', {
      icon: '❌',
      style: {
        borderRadius: '10px',
        background: '#000',
        color: '#fff',
      },
    });
    setCartData(newCart);
  };

  const cartItem = () => {
    return cartData && cartData.length > 0 ? (
      localData.map((item) => (
        <>
          <div
            className="flex flex-row justify-between items-center w-full"
            key={item.uniqueID}
          >
            <div className="flex flex-row justify-center items-center">
              <Image
                src={item.image}
                alt={item.name}
                height={100}
                width={100}
              />
              <div className="flex flex-col justify-center items-start ml-5">
                <h1 className="text-lg font-bold">{item.name}</h1>
                <h1 className="text-sm ">Rp{numberDot(item.price)}</h1>
                <h1 className="text-sm text-gray-500">{item.size}</h1>
              </div>
            </div>
            <div className="flex flex-row justify-center items-end md:items-center">
              <button
                className="rounded-full w-8 h-8 flex justify-center items-center border border-black hover:text-white hover:bg-black"
                onClick={() => deleteItem(item.idCart)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-trash-fill"
                  viewBox="0 0 16 16"
                >
                  <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
                </svg>
              </button>
            </div>
          </div>
          <hr className="border-gray-300 w-full m-2" />
        </>
      ))
    ) : (
      <div className="flex items-center justify-center h-full">
        <p className="text-lg">No items in cart</p>
      </div>
    );
  };

  return (
    <>
      <button onClick={handleCart}>
        <div className="relative">
          <Image src={Cart} alt="cart" height={30} width={30} />
          <div className="absolute -top-1 -right-2 w-4 h-4 bg-red-500 rounded-full flex items-center justify-center">
            <span className="text-white text-xs">{lenTotal}</span>
          </div>
        </div>
      </button>
      {isCart ? (
        <>
          <div
            className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-50"
            onClick={handleCart}
          ></div>
          <div className="fixed top-0 right-0 w-10/12 md:w-1/3 h-full bg-white z-50 ">
            <div className="flex flex-row justify-between items-center p-5">
              <h1 className="text-2xl font-bold">Cart</h1>
              <button onClick={handleCart}>
                <Image src={Close} alt="cart" height={17} width={17} />
              </button>
            </div>
            <hr className="border-gray-300" />
            <div className="overflow-y-scroll  md:h-4/6">
              <div className="flex flex-col justify-center items-center p-5">
                {cartItem()}
              </div>
            </div>
            <div className="flex flex-col justify-center items-center p-5">
              {cartData && cartData.length > 0 ? (
                <>
                  <div className="flex flex-row justify-between items-center w-full mt-2 md:mt-5">
                    <h1 className="text-lg font-bold">Total</h1>
                    <h1 className="text-lg font-bold">
                      Rp{numberDot(totalPrice)}
                    </h1>
                  </div>
                  <button className="w-full bg-black text-white py-3 rounded-lg mt-5 font-bold hover:bg-white hover:text-black border border-black">
                    Checkout
                  </button>
                </>
              ) : (
                ''
              )}
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}
