import Head from 'next/head';

export default function Products() {
  return (
    <div>
      <Head>
        <title>berbaju.co | About</title>
      </Head>
      <div className="container-default">
        <div className="m-12 lg:mt-7 flex flex-col-reverse md:flex-row pb-5 xl:pb-16 border-b border-black">
          <div className="flex flex-col justify-end md:w-[45%] md:pr-10 lg:pr-20 mt-8 md:mt-0 ">
            <p className="text-sm my-4" id="past">
              Our Products
            </p>

            <h2 className="text-5xl md:text-6xl lg:text-7xl leading-[1.1] mb-6 font-bold">
              Aesthetic since 2022.
            </h2>

            <p className="text-md md:text-lg font-light leading-[1.4]">
              Every fashion journey is different. Ours started in 2022,
              pioneering a national shift away from prescription pads and
              towards a more aesthethic approach to fashion. Since then, we’ve
              been known for nourishing the most aesthethic-obsessed humans on
              the planet.
            </p>
          </div>
          <div className="md:w-[55%] drop-shadow-lg md:mt-8 relative hover:scale-110 transition duration-300 ease-in-out">
            <img
              className="object-cover rounded-md  h-[157px] sm:h-[285px] md:h-[372px] w-2/3 z-10 mb-10 lg:mb-20"
              src="/oldshop.jpg"
            ></img>
            <img
              className="absolute w-1/2 object-cover h-[150px] sm:h-[257px] md:h-[348px] rounded-md z-negative right-0 bottom-0 "
              src="/side-view.jpg"
            ></img>
          </div>
        </div>
        <div className="m-12">
          <div className="items-end md:flex">
            <div className="mb-4 lg:mb-0 md:w-1/2">
              <p className="text-sm my-4">Meet the Team</p>
              <h2 className="text-5xl font-bold leading-none lg:text-6xl">
                Behind this marvellous brand.
              </h2>
            </div>
            <p className="font-light md:w-1/2 lg:text-lg md:ml-10">
              We exist to help everyone become the better version of themselves.
              That’s why product is worn by everyone, from the most aesthethic
              to the most aesthethic-obsessed. We’re a team of designers,
              engineers, and aesthethic-obsessed humans who are passionate about
              making the best products for everyone.
            </p>
          </div>
          <div className="pb-12 mt-8 md:mt-10 lg:mt-12 xl:mt-14 overflow-hidden">
            <div className="lg:mx-auto">
              <div className="relative flex items-center">
                <ul className="relative flex flex-col md:flex-row gap-8 max-h-fit">
                  <li className="relative border border-black rounded-md p-6 xl:p-12 flex flex-col justify-evenly">
                    <div className="flex flex-row items-center gap-10">
                      <div className="md:h-24 mt-5 overflow-hidden rounded-full">
                        <img className="w-24" src="/kiko.jpg"></img>
                      </div>
                      <h4 className="text-xl font-bold lg:text-2xl xl:text-3xl w-64">
                        Muhammad Kiko Aulia Reiki
                      </h4>
                    </div>

                    <p className="flex mt-6 font-light lg:text-base">
                      A passionate informatics student that interest in Data
                      Science and Frontend Web Development. Also attend several
                      UI/UX Competition and won once. Entered programming world
                      since 2016 and like to learn about data engineering and
                      machine learning.
                    </p>
                  </li>
                  <li className="relative border border-black rounded-md p-6 xl:p-12 flex flex-col justify-evenly">
                    <div className="flex flex-row items-center gap-10">
                      <div className="md:h-24 mt-5 overflow-hidden  rounded-full">
                        <img className="w-24" src="/agil.jpg"></img>
                      </div>
                      <h4 className="text-xl font-bold lg:text-2xl xl:text-3xl w-64">
                        Muhamad Agil Fachrian
                      </h4>
                    </div>

                    <p className="flex mt-6 font-light lg:text-base">
                      Information technology students who are interested in
                      blockchain and cybersecurity that have participated in
                      several CTF competitions and are looking for bugs in
                      crowdsourced or open programs. Currently studying
                      blockchain for my final project.
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
