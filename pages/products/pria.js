import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import Cards from '../../components/Cards/Cards';
import initData from '../../data';

export default function Pria({ data }) {
  return (
    <div>
      <Head>
        <title>berbaju.co | Products Pria</title>
      </Head>
      <nav
        className="!py-5 md:py-6 xl:py-7 text-sm container-default"
        role="navigation"
        aria-label="breadcrumbs"
      >
        <ol className="flex">
          <li className="after:content-['\002F'] after:px-0.5 last:font-bold last:after:content-['']">
            <Link href="/" title="Home">
              Home
            </Link>
          </li>
          <li className="after:content-['\002F'] after:px-0.5 last:font-bold last:after:content-['']">
            <Link href="/products" aria-current="page">
              Products
            </Link>
          </li>
          <li className="after:content-['\002F'] after:px-0.5 last:font-bold last:after:content-['']">
            <Link href="/products/pria" aria-current="page">
              Pria
            </Link>
          </li>
        </ol>
      </nav>
      <div className="pt-3 container-default md:pt-0">
        <div className="border-b border-grey-border">
          <h1 className="pb-3 md:pb-4 text-4xl font-bold md:text-5xl lg:text-7xl">
            Shop All
          </h1>
          <p className="pr-6 text-base font-light lg:text-lg md:pr-0">
            Explore a holistic lineup of t-shirts, shirts, and other
            aesthethic-oriented goodies — guaranteed and true to our standards
            to help you keep fashionable.
          </p>
          <p className="items-center hidden md:flex mt-5">
            <span className="flex-shrink-0 mr-3 font-bold text-tiny">
              Shop By: Category
            </span>
            <span className="inline-block w-full h-1 border-b"></span>
          </p>
          <ul className="flex mb-5 md:mb-0 grid-cols-5 gap-4 py-4 pt-7 md:py-6 overflow-x-auto no-scrollbar md:grid lg:gap-5 2xl:gap-8 overflow-right md:overflow-normal pr-12.5 md:border-0">
            <li className="mr-2.5 md:mr-0 relative md:h-14 lg:h-21.75 xl:h-32 md:rounded-md 2xl:rounded-big md:overflow-hidden">
              <Link className="overflow-hidden group" href="/products">
                <div className="inset-0 flex items-center justify-center md:absolute md:bg-black  cursor-pointer">
                  <h3 className="md:text-[2vw] xl:text-2xl font-medium md:font-bold text-grey whitespace-nowrap ">
                    <span className="text-black md:text-white">Shop All</span>
                  </h3>
                </div>
              </Link>
            </li>
            <li className="mr-2.5 md:mr-0 relative md:h-14 lg:h-21.75 xl:h-32 md:rounded-md 2xl:rounded-big md:overflow-hidden">
              <Link href="/products/pria">
                <div className="overflow-hidden group">
                  <div className="hidden absolute z-20 w-full h-full gradient-black md:block"></div>
                  <div className="hidden object-cover w-full h-full duration-500 md:block group-hover:scale-110">
                    <Image
                      src="/man.jpg"
                      loading="lazy"
                      height={300}
                      width={300}
                    ></Image>
                  </div>
                  <div className="inset-0 flex items-center justify-center md:absolute">
                    <h3 className="md:text-[2vw] xl:text-2xl font-medium md:font-bold text-grey whitespace-nowrap ">
                      <span className="text-grey md:text-white">Pria</span>
                    </h3>
                  </div>
                </div>
              </Link>
            </li>
            <li className="mr-2.5 md:mr-0 relative md:h-14 lg:h-21.75 xl:h-32 md:rounded-md 2xl:rounded-big md:overflow-hidden">
              <Link href="/products/wanita">
                <div className="overflow-hidden group">
                  <div className="hidden absolute z-20 w-full h-full gradient-black md:block"></div>
                  <div className="hidden object-cover w-full h-full duration-500 md:block group-hover:scale-110">
                    <Image
                      src="/woman.jpg"
                      loading="lazy"
                      height={300}
                      width={300}
                    ></Image>
                  </div>
                  <div className="inset-0 flex items-center justify-center md:absolute">
                    <h3 className="md:text-[2vw] xl:text-2xl font-medium md:font-bold text-grey whitespace-nowrap ">
                      <span className="text-grey md:text-white">Wanita</span>
                    </h3>
                  </div>
                </div>
              </Link>
            </li>
          </ul>
        </div>
        <div className="w-full pt-5">
          <p className="text-sm">
            <span data-filter-results="">
              {data.filter((item) => item.category === 'Pria').length}{' '}
            </span>
            results
          </p>
        </div>
        <div className="flex flex-col items-start justify-center pb-12">
          <div className="flex flex-wrap justify-center md:justify-start gap-5 py-5">
            {data
              .filter((item) => item.category === 'Pria')
              .map((item) => (
                <Link href={`/products/${item.uniqueID}`} key={item.uniqueID}>
                  <div>
                    <Cards
                      key={item.uniqueID}
                      id={item.uniqueID}
                      desc={item.description}
                      name={item.name}
                      price={item.price}
                      image={item.image}
                    />
                  </div>
                </Link>
              ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps() {
  const data = initData;
  return {
    props: {
      data,
    },
  };
}
