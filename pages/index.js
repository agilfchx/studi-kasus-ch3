import { useState, useEffect } from 'react';
import Hero from '../components/Hero/Hero';
import Category from '../components/Category/Category';
import Cards from '../components/Cards/Cards';
import Testimoni from '../components/Testimoni/Testimoni';
import initData from '../data';
import Link from 'next/link';

export default function Home() {
  const [data, setData] = useState([]);
  useEffect(() => {
    setData(initData);
  }, []);

  return (
    <div>
      <Hero />
      <Category />
      <div className="flex flex-col items-center justify-center">
        <h1 className="text-4xl font-bold">Featured Products</h1>
        <div className="flex flex-wrap justify-center gap-5 mx-24 py-5">
          {data
            .filter((item) => item.category === 'Pria')
            .map((item) => (
              <a href={`/products/${item.uniqueID}`} key={item.uniqueID}>
                <Cards
                  key={item.uniqueID}
                  id={item.uniqueID}
                  desc={item.description}
                  name={item.name}
                  price={item.price}
                  image={item.image}
                />
              </a>
            ))}
        </div>
      </div>

      <hr className=" border-t border-gray-700 mx-24 mt-5" />
      <Testimoni />
    </div>
  );
}
