/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
    maxWidth: {
      '1/2': '50%',
      '1/3': '33%',
    },
    gridTemplateColumns: {
      1: 'repeat(1, minmax(0, 1fr))',
      5: 'repeat(5,minmax(0,1fr))',
      autoFit: 'repeat(auto-fit,minmax(0,1fr))',
    },
    blur: {
      xs: '2px',
    },
  },
  plugins: [],
};
